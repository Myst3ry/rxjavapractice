package com.myst3ry.rxjavapractice;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.myst3ry.data.datasource.local.ForecastsDatabase;
import com.myst3ry.rxjavapractice.di.component.AppComponent;
import com.myst3ry.rxjavapractice.di.component.DaggerAppComponent;
import com.myst3ry.rxjavapractice.di.module.AppModule;

public final class RxJavaPracticeApp extends Application {

    private AppComponent appComponent;
    private ForecastsDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        initDatabase();
        prepareDaggerComponents();
    }

    private void initDatabase() {
        database = Room.databaseBuilder(this, ForecastsDatabase.class, "forecasts_db")
                .build();
    }

    private void prepareDaggerComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public ForecastsDatabase getDatabase() {
        return database;
    }
}
