package com.myst3ry.rxjavapractice.view;

import com.myst3ry.rxjavapractice.data.model.ForecastDataModel;

public interface DetailView {

    void setForecastInfo(final ForecastDataModel forecast);
}
