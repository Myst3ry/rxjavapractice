package com.myst3ry.rxjavapractice.view;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.myst3ry.rxjavapractice.RxJavaPracticeApp;
import com.myst3ry.rxjavapractice.LinearSpacingItemDecoration;
import com.myst3ry.rxjavapractice.R;
import com.myst3ry.rxjavapractice.data.model.ForecastDataModel;
import com.myst3ry.rxjavapractice.presenter.MainPresenter;
import com.myst3ry.rxjavapractice.view.adapter.ForecastAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.weather_rec_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.refresher)
    SwipeRefreshLayout mRefresher;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.et_city_select)
    EditText mCitySelector;
    @BindView(R.id.et_days_select)
    EditText mDaysSelector;

    @Inject
    MainPresenter mPresenter;

    private ForecastAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((RxJavaPracticeApp)getApplication()).getAppComponent().mainSubComponent().inject(this);
        ButterKnife.bind(this);

        initAdapter();
        initRecyclerView();
        setRefreshListener();
        setDefaultValues();

        mPresenter.attachView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        preparePresenter();
    }

    private void preparePresenter() {
        mPresenter.getForecasts(mCitySelector.getText().toString());
        mPresenter.setCityChangeListener(mCitySelector, mDaysSelector);
        mPresenter.setDaysChangeListener(mDaysSelector, mCitySelector);
    }

    private void initAdapter() {
        mAdapter = new ForecastAdapter(id -> startActivity(ForecastDetailActivity.newExtraIntent(MainActivity.this, id)));
    }

    private void initRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(LinearSpacingItemDecoration.newBuilder()
                .spacing(getResources().getDimensionPixelSize(R.dimen.margin_half))
                .orientation(LinearLayoutManager.VERTICAL)
                .includeEdge(true)
                .build());
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setDefaultValues() {
        mCitySelector.setText(getString(R.string.text_city_default));
        mDaysSelector.setText(getString(R.string.text_days_default));
    }

    @Override
    public void setDaysCount(final String days) {
        mDaysSelector.setText(days);
    }

    @Override
    public void setForecasts(List<ForecastDataModel> forecasts) {
        mAdapter.setForecastList(forecasts);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.err_no_data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.GONE) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideRefresher() {
        if (mRefresher != null && mRefresher.isRefreshing()) {
            mRefresher.setRefreshing(false);
        }
    }

    private void setRefreshListener() {
        mRefresher.setOnRefreshListener(() -> mPresenter.getForecastsUpdates(mCitySelector.getText().toString(),
                Integer.valueOf(mDaysSelector.getText().toString())));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isDestroyed()) {
            mPresenter.detachView();
            mPresenter.disposeAll();
        }
    }
}
