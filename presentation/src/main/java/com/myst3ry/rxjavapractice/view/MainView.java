package com.myst3ry.rxjavapractice.view;

import com.myst3ry.rxjavapractice.data.model.ForecastDataModel;

import java.util.List;

public interface MainView {

    void setForecasts(final List<ForecastDataModel> forecasts);

    void showProgressBar();

    void hideProgressBar();

    void hideRefresher();

    void showError();

    void setDaysCount(final String days);
}
