package com.myst3ry.rxjavapractice.di.module;

import android.content.Context;

import com.myst3ry.data.datasource.local.ForecastsDatabase;
import com.myst3ry.rxjavapractice.RxJavaPracticeApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DatabaseModule {

    @Provides
    @Singleton
    ForecastsDatabase providesDatabase(final Context context) {
        return ((RxJavaPracticeApp)context.getApplicationContext()).getDatabase();
    }
}
