package com.myst3ry.rxjavapractice.di.module;

import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.datasource.local.ForecastsDatabase;
import com.myst3ry.data.datasource.remote.ApiMapper;
import com.myst3ry.data.repository.ForecastRepositoryImpl;
import com.myst3ry.domain.repository.ForecastRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DataModule {

    @Provides
    @Singleton
    ForecastRepository providesForecastRepository(final NetworkManager networkManager,
                                                  final ForecastsDatabase database,
                                                  final ApiMapper apiMapper) {
        return new ForecastRepositoryImpl(networkManager, database, apiMapper);
    }
}
