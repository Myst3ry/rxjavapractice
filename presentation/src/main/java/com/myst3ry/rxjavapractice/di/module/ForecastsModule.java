package com.myst3ry.rxjavapractice.di.module;

import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.presenter.MainPresenter;
import com.myst3ry.domain.interactor.GetForecastsCase;
import com.myst3ry.domain.repository.ForecastRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ForecastsModule {

    @Provides
    @PerActivity
    MainPresenter providesMainPresenter(final GetForecastsCase forecastsUseCase) {
        return new MainPresenter(forecastsUseCase);
    }

    @Provides
    @PerActivity
    GetForecastsCase providesForecastsCase(final ForecastRepository repository) {
        return new GetForecastsCase(repository);
    }
}
