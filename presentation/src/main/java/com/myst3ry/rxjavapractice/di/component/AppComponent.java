package com.myst3ry.rxjavapractice.di.component;

import com.myst3ry.rxjavapractice.di.module.AppModule;
import com.myst3ry.rxjavapractice.di.module.DataModule;
import com.myst3ry.rxjavapractice.di.module.DatabaseModule;
import com.myst3ry.rxjavapractice.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {AppModule.class, DataModule.class, DatabaseModule.class, NetworkModule.class})
public interface AppComponent {

    MainSubComponent mainSubComponent();

    DetailSubComponent detailSubComponent();
}
