package com.myst3ry.rxjavapractice.di.module;

import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.presenter.DetailPresenter;
import com.myst3ry.domain.interactor.GetDailyForecastCase;
import com.myst3ry.domain.repository.ForecastRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DailyForecastModule {

    @Provides
    @PerActivity
    DetailPresenter providesDetailPresenter(final GetDailyForecastCase dailyForecastUseCase) {
        return new DetailPresenter(dailyForecastUseCase);
    }

    @Provides
    @PerActivity
    GetDailyForecastCase providesDailyForecastCase(final ForecastRepository repository) {
        return new GetDailyForecastCase(repository);
    }
}
