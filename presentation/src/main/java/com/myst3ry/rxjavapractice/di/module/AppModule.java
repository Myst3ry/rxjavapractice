package com.myst3ry.rxjavapractice.di.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;

import com.myst3ry.rxjavapractice.presenter.DetailPresenter;
import com.myst3ry.rxjavapractice.presenter.MainPresenter;
import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.ResourcesManager;
import com.myst3ry.domain.interactor.GetDailyForecastCase;
import com.myst3ry.domain.interactor.GetForecastsCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {

    private final Application mApplication;

    public AppModule(final Application application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Context providesAppContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources providesResources(final Context context) {
        return context.getResources();
    }

    @Provides
    @Singleton
    ResourcesManager providesResourcesManager(final Resources resources) {
        return new ResourcesManager(resources);
    }

    @Provides
    @Singleton
    ConnectivityManager providesConnectivityManager(final Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    NetworkManager providesNetworkManager(final ConnectivityManager manager) {
        return new NetworkManager(manager);
    }
}
