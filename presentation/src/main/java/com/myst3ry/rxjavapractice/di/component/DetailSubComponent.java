package com.myst3ry.rxjavapractice.di.component;

import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.di.module.DailyForecastModule;
import com.myst3ry.rxjavapractice.view.ForecastDetailActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent (modules = {DailyForecastModule.class})
public interface DetailSubComponent {

    void inject(final ForecastDetailActivity detailActivity);
}
