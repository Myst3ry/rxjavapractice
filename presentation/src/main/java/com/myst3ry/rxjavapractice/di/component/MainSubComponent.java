package com.myst3ry.rxjavapractice.di.component;

import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.di.module.ForecastsModule;
import com.myst3ry.rxjavapractice.view.MainActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent (modules = {ForecastsModule.class})
public interface MainSubComponent {

    void inject(final MainActivity mainActivity);
}
