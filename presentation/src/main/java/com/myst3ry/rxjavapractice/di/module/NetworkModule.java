package com.myst3ry.rxjavapractice.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myst3ry.data.ResourcesManager;
import com.myst3ry.data.datasource.remote.ApiMapper;
import com.myst3ry.data.datasource.remote.ForecastsApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class NetworkModule {

    @Provides
    @Singleton
    Gson providesGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(final Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(ForecastsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ForecastsApi providesApiService(final Retrofit retrofit) {
        return retrofit.create(ForecastsApi.class);
    }

    @Provides
    @Singleton
    ApiMapper providesApiMapper(final ResourcesManager manager, final ForecastsApi api) {
        return new ApiMapper(manager, api);
    }
}
