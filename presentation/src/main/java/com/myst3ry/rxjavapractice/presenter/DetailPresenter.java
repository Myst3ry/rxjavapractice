package com.myst3ry.rxjavapractice.presenter;

import android.util.Log;

import com.myst3ry.rxjavapractice.data.mapper.ForecastDataModelMapper;
import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.view.DetailView;
import com.myst3ry.domain.interactor.GetDailyForecastCase;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public final class DetailPresenter extends BasePresenter<DetailView> {

    private static final String TAG = "DetailPresenter";

    private final GetDailyForecastCase mGetDailyForecastUseCase;

    @Inject
    public DetailPresenter(final GetDailyForecastCase dailyForecastUseCase) {
        this.mGetDailyForecastUseCase = dailyForecastUseCase;
    }

    public void getForecast(final int epoch) {
        addDisposable(mGetDailyForecastUseCase.execute(epoch)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map(ForecastDataModelMapper::transform)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mView::setForecastInfo,
                        t -> Log.e(TAG, t.getLocalizedMessage())));
    }
}
