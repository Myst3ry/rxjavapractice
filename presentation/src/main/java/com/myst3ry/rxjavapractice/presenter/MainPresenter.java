package com.myst3ry.rxjavapractice.presenter;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;

import com.myst3ry.rxjavapractice.data.mapper.ForecastDataModelMapper;
import com.myst3ry.rxjavapractice.di.PerActivity;
import com.myst3ry.rxjavapractice.view.MainView;
import com.myst3ry.domain.interactor.GetForecastsCase;
import com.myst3ry.domain.model.Forecast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public final class MainPresenter extends BasePresenter<MainView> {

    private static final String TAG = "MainPresenter";
    private static final int MAX_DAYS_COUNT = 5;

    private final GetForecastsCase mGetForecastsUseCase;
    private int mDaysCount;

    @Inject
    public MainPresenter(final GetForecastsCase forecastsUseCase) {
        this.mGetForecastsUseCase = forecastsUseCase;
    }

    public void getForecasts(final String city) {
        mView.showProgressBar();
        requestForecasts(city);
    }

    public void getForecastsUpdates(final String city, final int daysCount) {
        this.mDaysCount = daysCount;
        requestForecasts(city);
    }

    private void requestForecasts(final String city) {
        addDisposable(mGetForecastsUseCase.execute(city)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map((forecasts) -> ForecastDataModelMapper.transform(filterForecasts(forecasts)))
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> {
                    mView.hideProgressBar();
                    mView.hideRefresher();
                })
                .subscribe(forecasts -> {
                    if (!forecasts.isEmpty()) {
                        mView.setForecasts(forecasts);
                    } else {
                        mView.showError();
                    }
                }, t -> Log.e(TAG, t.getLocalizedMessage())));
    }

    private List<Forecast> filterForecasts(final List<Forecast> forecasts) {
        List<Forecast> filtered = new ArrayList<>(mDaysCount);
        if (mDaysCount != forecasts.size() && mDaysCount > 0) {
            for (int i = 0; i < mDaysCount; i++) {
                filtered.add(forecasts.get(i));
            }
        } else {
            filtered = forecasts;
        }
        return filtered;
    }

    public void setCityChangeListener(final EditText citySelector, final EditText daysSelector) {
        citySelector.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                citySelector.setOnKeyListener((v, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        mDaysCount = Integer.valueOf(daysSelector.getText().toString());
                        requestForecasts(s.toString());
                    }
                    return false;
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void setDaysChangeListener(final EditText daysSelector, final EditText citySelector) {
        daysSelector.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                daysSelector.setOnKeyListener((v, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if (Integer.valueOf(s.toString()) > MAX_DAYS_COUNT) {
                            mDaysCount = MAX_DAYS_COUNT;
                            mView.setDaysCount(String.valueOf(MAX_DAYS_COUNT));
                        } else {
                            mDaysCount = Integer.valueOf(s.toString());
                        }
                        requestForecasts(citySelector.getText().toString());
                    }
                    return false;
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}
