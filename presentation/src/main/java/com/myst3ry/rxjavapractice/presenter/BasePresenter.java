package com.myst3ry.rxjavapractice.presenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

abstract class BasePresenter<T> {

    protected T mView;
    protected CompositeDisposable disposables = new CompositeDisposable();

    protected void addDisposable(final Disposable disposable) {
        disposables.add(disposable);
    }

    public void attachView(T view) {
        this.mView = view;
    }

    public void detachView() {
        this.mView = null;
    }

    public void disposeAll() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }
}
