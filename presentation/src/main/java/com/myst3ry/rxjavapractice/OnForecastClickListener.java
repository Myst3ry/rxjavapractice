package com.myst3ry.rxjavapractice;

public interface OnForecastClickListener {

    void onForecastClick(final int id);
}
