package com.myst3ry.rxjavapractice.data.model;

public final class ForecastDataModel {

    private int mId;
    private String mDayOfWeek;
    private String mDate;
    private String mTemperatureMax;
    private String mTemperatureMin;
    private String mTemperatureAvg;
    private String mDayPhrase;
    private String mNightPhrase;
    private String mCity;

    public ForecastDataModel(int id, String dayOfWeek, String date, String maxTemp, String minTemp,
                             String avgTemp, String dayPhrase, String nightPhrase, String city) {
        this.mId = id;
        this.mDayOfWeek = dayOfWeek;
        this.mDate = date;
        this.mTemperatureMax = maxTemp;
        this.mTemperatureMin = minTemp;
        this.mTemperatureAvg = avgTemp;
        this.mDayPhrase = dayPhrase;
        this.mNightPhrase = nightPhrase;
        this.mCity = city;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getDayOfWeek() {
        return mDayOfWeek;
    }

    public void setDayOfWeek(final String dayOfWeek) {
        this.mDayOfWeek = dayOfWeek;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(final String date) {
        this.mDate = date;
    }

    public String getTemperatureMax() {
        return mTemperatureMax;
    }

    public void setTemperatureMax(final String temperatureMax) {
        this.mTemperatureMax = temperatureMax;
    }

    public String getTemperatureMin() {
        return mTemperatureMin;
    }

    public void setTemperatureMin(final String temperatureMin) {
        this.mTemperatureMin = temperatureMin;
    }

    public String getTemperatureAvg() {
        return mTemperatureAvg;
    }

    public void setTemperatureAvg(final String temperatureAvg) {
        this.mTemperatureAvg = temperatureAvg;
    }

    public String getDayPhrase() {
        return mDayPhrase;
    }

    public void setDayPhrase(final String dayPhrase) {
        this.mDayPhrase = dayPhrase;
    }

    public String getNightPhrase() {
        return mNightPhrase;
    }

    public void setNightPhrase(final String nightPhrase) {
        this.mNightPhrase = nightPhrase;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(final String city) {
        this.mCity = city;
    }

    @Override
    public String toString() {
        return "ForecastDataModel {" +
                "mDayOfWeek='" + mDayOfWeek + '\'' +
                ", mDate='" + mDate + '\'' +
                ", mTemperatureMax='" + mTemperatureMax + '\'' +
                ", mTemperatureMin='" + mTemperatureMin + '\'' +
                ", mTemperatureAvg='" + mTemperatureAvg + '\'' +
                ", mDayPhrase='" + mDayPhrase + '\'' +
                ", mNightPhrase='" + mNightPhrase + '\'' +
                ", mCity='" + mCity + '\'' +
                '}';
    }
}
