package com.myst3ry.rxjavapractice.data.mapper;

import com.myst3ry.rxjavapractice.data.model.ForecastDataModel;
import com.myst3ry.domain.model.Forecast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class ForecastDataModelMapper {

    private static final int WEEKDAY_ZERO_LENGTH = 0;
    private static final int WEEKDAY_MIN_LENGTH = 1;
    private static final long UNIX_TIME_MULTIPLIER = 1000L;

    private static final String DAY_OF_WEEK_FORMAT = "EEEE";
    private static final String INPUT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String OUTPUT_DATE_FORMAT = "dd MMMM";

    private static final String FAHRENHEIT_UNIT = "F";
    private static final String DEGREE_UNIT = "C";

    public static ForecastDataModel transform(final Forecast forecast) {
        return new ForecastDataModel(
                forecast.getEpochDate(),
                getDayOfWeek(forecast.getEpochDate()),
                parseDate(forecast.getDate()),
                getTempUnitString(forecast.getTemperatureMax(), forecast.getUnit()),
                getTempUnitString(forecast.getTemperatureMin(), forecast.getUnit()),
                getAvgTempString(forecast.getTemperatureMin(), forecast.getTemperatureMax(), forecast.getUnit()),
                forecast.getDayPhrase(),
                forecast.getNightPhrase(),
                forecast.getCity());
    }

    public static List<ForecastDataModel> transform(final List<Forecast> forecasts) {
        final List<ForecastDataModel> dataModels = new ArrayList<>();
        for (final Forecast forecast : forecasts) {
            dataModels.add(transform(forecast));
        }
        return dataModels;
    }

    private static String getDayOfWeek(final int epochDate) {
        final SimpleDateFormat outputFormat = new SimpleDateFormat(DAY_OF_WEEK_FORMAT, Locale.getDefault());
        final String weekday = outputFormat.format(new Date(epochDate * UNIX_TIME_MULTIPLIER));
        return reformatWeekday(weekday);

    }

    private static String parseDate(final String date) {
        final SimpleDateFormat inputFormat = new SimpleDateFormat(INPUT_DATE_FORMAT, Locale.getDefault());
        final SimpleDateFormat outputFormat = new SimpleDateFormat(OUTPUT_DATE_FORMAT, Locale.getDefault());
        try {
            return outputFormat.format(inputFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    private static String reformatWeekday(final String input) {
        String output = null;
        if (input.length() > WEEKDAY_MIN_LENGTH) {
            output = input.substring(WEEKDAY_ZERO_LENGTH, WEEKDAY_MIN_LENGTH)
                    .toUpperCase()
                    .concat(input.substring(WEEKDAY_MIN_LENGTH, input.length()));
        }
        return output;
    }

    private static String getAvgTempString(final int min, final int max, final String unit) {
        return String.format("%s - %s", getTempUnitString(min, unit), getTempUnitString(max, unit));
    }

    private static String getTempUnitString(final int temp, final String unit) {
        return unit.equalsIgnoreCase(DEGREE_UNIT) ? String.format(Locale.getDefault(), "%d \u2103", temp)
                : unit.equalsIgnoreCase(FAHRENHEIT_UNIT) ? String.format(Locale.getDefault(), "%d \u2109", temp)
                : String.valueOf(temp);
    }

    private ForecastDataModelMapper() {
    }
}
