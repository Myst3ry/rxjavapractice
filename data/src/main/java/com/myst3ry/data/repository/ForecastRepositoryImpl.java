package com.myst3ry.data.repository;

import android.arch.persistence.room.Transaction;

import com.myst3ry.data.NetworkManager;
import com.myst3ry.data.datasource.local.ForecastsDatabase;
import com.myst3ry.data.datasource.local.mapper.ForecastDataMapper;
import com.myst3ry.data.datasource.local.mapper.ForecastEntityMapper;
import com.myst3ry.data.datasource.local.entity.ForecastEntity;
import com.myst3ry.data.datasource.remote.ApiMapper;
import com.myst3ry.domain.model.Forecast;
import com.myst3ry.domain.repository.ForecastRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public final class ForecastRepositoryImpl implements ForecastRepository {

    private final NetworkManager mNetworkManager;
    private final ForecastsDatabase mDatabase;
    private final ApiMapper mApiMapper;

    @Inject
    public ForecastRepositoryImpl(final NetworkManager networkManager,
                                  final ForecastsDatabase database,
                                  final ApiMapper apiMapper) {
        this.mNetworkManager = networkManager;
        this.mDatabase = database;
        this.mApiMapper = apiMapper;
    }

    @Override
    public Observable<List<Forecast>> requestAllForecasts(final String city) {
        Observable<List<ForecastEntity>> forecasts;

        if (mNetworkManager.isNetworkAvailable()) {
            forecasts = mApiMapper.getForecastsRemote(city)
                    .observeOn(Schedulers.computation())
                    .map(models -> ForecastEntityMapper.transform(models, city))
                    .doOnNext(this::saveForecasts);
        } else {
            forecasts = mDatabase.getForecastDao()
                    .getForecasts(city)
                    .toObservable();
        }

        return forecasts.map(ForecastDataMapper::transform);
    }

    @Override
    public Observable<Forecast> requestDailyForecast(int epoch) {
        return mDatabase.getForecastDao()
                .getDailyForecast(epoch)
                .toObservable()
                .map(ForecastDataMapper::transform);
    }

    @Transaction
    private void saveForecasts(final List<ForecastEntity> entities) {
        mDatabase.getForecastDao().deleteAllForecasts();
        for (final ForecastEntity entity : entities) {
            mDatabase.getForecastDao().saveForecast(entity);
        }
    }
}
