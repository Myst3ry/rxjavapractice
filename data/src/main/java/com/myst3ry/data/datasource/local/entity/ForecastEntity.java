package com.myst3ry.data.datasource.local.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Model class that represents a Forecast entity in the data layer.
 */
@Entity(tableName = "forecasts")
public final class ForecastEntity {

    @PrimaryKey
    @ColumnInfo(name = "epoch")
    private int mEpochDate;

    @ColumnInfo(name = "date")
    private String mDate;

    @ColumnInfo(name = "temp_max")
    private int mTemperatureMax;

    @ColumnInfo(name = "temp_min")
    private int mTemperatureMin;

    @ColumnInfo(name = "unit")
    private String mUnit;

    @ColumnInfo(name = "phrase_day")
    private String mDayPhrase;

    @ColumnInfo(name = "phrase_night")
    private String mNightPhrase;

    @ColumnInfo(name = "city")
    private String mCity;

    public ForecastEntity(int mEpochDate, String mDate, int mTemperatureMax, int mTemperatureMin,
                          String mUnit, String mDayPhrase, String mNightPhrase, String mCity) {
        this.mEpochDate = mEpochDate;
        this.mDate = mDate;
        this.mTemperatureMax = mTemperatureMax;
        this.mTemperatureMin = mTemperatureMin;
        this.mUnit = mUnit;
        this.mDayPhrase = mDayPhrase;
        this.mNightPhrase = mNightPhrase;
        this.mCity = mCity;
    }

    public int getEpochDate() {
        return mEpochDate;
    }

    public void setEpochDate(final int epochDate) {
        this.mEpochDate = epochDate;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(final String date) {
        this.mDate = date;
    }

    public int getTemperatureMax() {
        return mTemperatureMax;
    }

    public void setTemperatureMax(final int temperatureMax) {
        this.mTemperatureMax = temperatureMax;
    }

    public int getTemperatureMin() {
        return mTemperatureMin;
    }

    public void setTemperatureMin(final int temperatureMin) {
        this.mTemperatureMin = temperatureMin;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(final String tempUnit) {
        this.mUnit = tempUnit;
    }

    public String getDayPhrase() {
        return mDayPhrase;
    }

    public void setDayPhrase(final String dayPhrase) {
        this.mDayPhrase = dayPhrase;
    }

    public String getNightPhrase() {
        return mNightPhrase;
    }

    public void setNightPhrase(final String nightPhrase) {
        this.mNightPhrase = nightPhrase;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(final String city) {
        this.mCity = city;
    }

    @Override
    public String toString() {
        return "ForecastEntity {" +
                "EpochDate=" + mEpochDate +
                ", Date='" + mDate + '\'' +
                ", TemperatureMax=" + mTemperatureMax +
                ", TemperatureMin=" + mTemperatureMin +
                ", TempUnit=" + mUnit + '\'' +
                ", DayPhrase='" + mDayPhrase + '\'' +
                ", NightPhrase='" + mNightPhrase + '\'' +
                ", City='" + mCity + '\'' +
                '}';
    }
}
