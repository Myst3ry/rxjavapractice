package com.myst3ry.data.datasource.remote;

import com.myst3ry.data.datasource.remote.model.City;
import com.myst3ry.data.datasource.remote.model.ForecastResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ForecastsApi {

    String BASE_URL = "http://dataservice.accuweather.com/";

    @GET("forecasts/v{version}/daily/5day/{locationKey}")
    Observable<ForecastResponse> getWeatherForecast(@Path("version") final int apiVersion,
                                                @Path("locationKey") final String locationKey,
                                                @Query("apikey") final String apiKey,
                                                @Query("language") final String language,
                                                @Query("metric") final boolean isMetricEnabled);

    @GET("locations/v{version}/cities/search")
    Observable<List<City>> getCitiesByQuery(@Path("version") final int apiVersion,
                                            @Query("apikey") final String apiKey,
                                            @Query("q") final String cityName,
                                            @Query("language") final String language);
}

