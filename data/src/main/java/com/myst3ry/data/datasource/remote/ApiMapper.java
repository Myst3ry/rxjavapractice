package com.myst3ry.data.datasource.remote;

import android.support.annotation.Nullable;
import android.util.Log;

import com.myst3ry.data.ResourcesManager;
import com.myst3ry.data.datasource.remote.model.City;
import com.myst3ry.data.datasource.remote.model.ForecastModel;
import com.myst3ry.data.datasource.remote.model.ForecastResponse;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public final class ApiMapper {

    private static final int FORECASTS_API_VERSION = 1;
    private static final int LOCATIONS_API_VERSION = 1;
    private static final String API_KEY = "FjAGIuKI7rqIHxbHenTmPaWsg1YKYCLU";
    private static final String TAG = "ApiMapper";

    private final ResourcesManager mResourcesManager;
    private final ForecastsApi mService;

    @Inject
    public ApiMapper(final ResourcesManager resourcesManager, final ForecastsApi service) {
        this.mResourcesManager = resourcesManager;
        this.mService = service;
    }

    public Observable<List<ForecastModel>> getForecastsRemote(@Nullable final String cityQuery) {
        return mService.getCitiesByQuery(LOCATIONS_API_VERSION, API_KEY,
                cityQuery != null ? cityQuery : mResourcesManager.getCity(), mResourcesManager.getLanguage())
                .observeOn(Schedulers.computation())
                .flatMap(cities -> {
                    final City city = cities != null && !cities.isEmpty() ? cities.get(0) : null;
                    return mService.getWeatherForecast(FORECASTS_API_VERSION, Objects.requireNonNull(city).getKey(),
                            API_KEY, mResourcesManager.getLanguage(), mResourcesManager.isMetricEnabled())
                            .map(ForecastResponse::getForecasts);
                })
                .doOnError(throwable -> Log.e(TAG, throwable.getLocalizedMessage()));
    }
}