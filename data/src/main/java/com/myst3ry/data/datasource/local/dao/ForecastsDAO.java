package com.myst3ry.data.datasource.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.myst3ry.data.datasource.local.entity.ForecastEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

@Dao
public interface ForecastsDAO {

    @Query("select * from forecasts where city = :city")
    Flowable<List<ForecastEntity>> getForecasts(final String city);

    @Query("select * from forecasts where epoch = :epochDate")
    Flowable<ForecastEntity> getDailyForecast(final int epochDate);

    @Query("delete from forecasts")
    void deleteAllForecasts();

    @Insert
    void saveForecast(final ForecastEntity entity);


}