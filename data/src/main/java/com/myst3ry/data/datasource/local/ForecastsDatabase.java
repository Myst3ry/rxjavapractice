package com.myst3ry.data.datasource.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.myst3ry.data.datasource.local.dao.ForecastsDAO;
import com.myst3ry.data.datasource.local.entity.ForecastEntity;

@Database(entities = {ForecastEntity.class}, version = 1)
public abstract class ForecastsDatabase extends RoomDatabase {

    public abstract ForecastsDAO getForecastDao();
}