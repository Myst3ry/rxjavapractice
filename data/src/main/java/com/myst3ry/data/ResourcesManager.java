package com.myst3ry.data;

import android.content.res.Resources;

import javax.inject.Inject;

public final class ResourcesManager {

    private final Resources mResources;

    @Inject
    public ResourcesManager(final Resources resources) {
        this.mResources = resources;
    }

    public String getCountry() {
        return mResources.getString(R.string.country);
    }

    public String getCity() {
        return mResources.getString(R.string.city);
    }

    public String getLanguage() {
        return mResources.getString(R.string.language);
    }

    public boolean isMetricEnabled() {
        return mResources.getBoolean(R.bool.metrics);
    }
}
