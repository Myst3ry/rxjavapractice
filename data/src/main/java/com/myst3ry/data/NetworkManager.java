package com.myst3ry.data;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

public final class NetworkManager {

    private final ConnectivityManager mConnectivityManager;

    @Inject
    public NetworkManager(final ConnectivityManager connectivityManager) {
        this.mConnectivityManager = connectivityManager;
    }

    public boolean isNetworkAvailable() {
        final NetworkInfo activeNetworkInfo = mConnectivityManager != null ? mConnectivityManager.getActiveNetworkInfo() : null;
        return (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting());
    }

}
