package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Forecast;
import com.myst3ry.domain.repository.ForecastRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public final class GetForecastsCase {

    private final ForecastRepository mForecastRepository;

    @Inject
    public GetForecastsCase(final ForecastRepository repository) {
        this.mForecastRepository = repository;
    }

    public Observable<List<Forecast>> execute(final String city) {
        return mForecastRepository.requestAllForecasts(city);
    }
}
