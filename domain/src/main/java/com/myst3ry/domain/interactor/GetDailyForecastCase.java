package com.myst3ry.domain.interactor;

import com.myst3ry.domain.model.Forecast;
import com.myst3ry.domain.repository.ForecastRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public final class GetDailyForecastCase {

    private final ForecastRepository mForecastRepository;

    @Inject
    public GetDailyForecastCase(final ForecastRepository repository) {
        this.mForecastRepository = repository;
    }

    public Observable<Forecast> execute(final int epoch) {
        return mForecastRepository.requestDailyForecast(epoch);
    }
}
