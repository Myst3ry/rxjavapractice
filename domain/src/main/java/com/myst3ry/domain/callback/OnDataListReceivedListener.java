package com.myst3ry.domain.callback;

import com.myst3ry.domain.model.Forecast;

import java.util.List;

public interface OnDataListReceivedListener {

    void onDataListReceived(final List<Forecast> dataList);
}
