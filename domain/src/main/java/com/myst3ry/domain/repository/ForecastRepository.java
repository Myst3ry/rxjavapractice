package com.myst3ry.domain.repository;

import com.myst3ry.domain.model.Forecast;

import java.util.List;

import io.reactivex.Observable;

public interface ForecastRepository {

    Observable<List<Forecast>> requestAllForecasts(final String city);

    Observable<Forecast> requestDailyForecast(final int epochDate);
}
